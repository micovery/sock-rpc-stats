var util = require("util"),
    _ = require("lodash"),
  log = require("util-logging"),
  logger =  new log.ConsoleLogger().setLevel(log.Level.INFO);

var isMetaKey = function(key) {
  return typeof key == "string" && (key.indexOf("$") == 0 || key.indexOf("_") == 0 || key.indexOf("_") == (key.length - 1));
};

var handleReadOps = function(op, o, k) {
  if (!o) {
    return undefined;
  }

  if (op == "get") {
    return o;
  }

  if (op == "keys") {
    try {
      return Object.keys(o).filter(function (key) {
        return !isMetaKey(key);
      });
    }
    catch(ex) {
      logger.severe("o = %s, k = %s", JSON.stringify(o), JSON.stringify(k));
      logger.severe(ex);
      return;
    }
  }

  if (op == "count") {
    try {
      return Object.keys(o).filter(function (key) {
        return !isMetaKey(key);
      }).length;
    }
    catch(ex) {
      logger.severe("o = %s, k = %s", JSON.stringify(o), JSON.stringify(k));
      logger.severe(ex);
      return;
    }
  }

  //array operations below
  if (!util.isArray(o)) {
    return undefined;
  }

  if (op == "shift") {
    return o.shift();
  }

  if (op == "pop") {
    return o.pop();
  }

  return undefined;
};

var readHandler = function(op, o, k) {
  if (typeof k != "string")  {
    return;
  }

  k = k.trim();

  //using dot notation
  k = k.replace(/\[(\w+)\]/g, '.#$1');  // convert indexes to properties
  k = k.replace(/\.+/g, "."); //collapse repeated dots
  k = k.replace(/^\./, ''); // strip leading dot
  k = k.replace(/\.$/, ''); // strip ending dot
  k = k.trim();

  if (k == "") {
    return handleReadOps(op, o, k);
  }


  var a = k.split(/\./);
  while (a.length) {
    var n = a.shift();

    if ((n.charAt(0) == "#")) {
      n = n.substr(1);
      if (isNaN(parseInt(n))) {
        //remove quotes if any
        n = n.trim();
        n = n.replace(/^"/,'');
        n = n.replace(/"$/,'');
      }
    }

    if (n in o) {
      o = o[n];
    } else {
      return;
    }
  }

  return handleReadOps(op, o, k);
};


var handleWriteOps = function(op, o, k, v) {
  if (op == "set") {
    o[k] = v;
    return true;
  }

  if (op == "merge") {
    o[k] = _.merge(o[k],v);
    return true;
  }

  //array operations operations below
  if (k in o && !util.isArray(o[k])) {
    return false;
  }

  if (!(k in o)) {
    o[k] = [];
  }

  if (op == "push") {
    o[k].push(v);
    return true;
  }

  if (op == "unshift") {
    o[k].unshift(v);
    return true;
  }


  return false;
};

var writeHandler = function(op, o, k, v) {

  if (typeof k != "string")  {
    return false;
  }

  k = k.trim();

  //using dot notation
  k = k.replace(/\[(\w+)\]/g, '.#$1.');  //add dot between array markers
  k = k.replace(/\.+/g, "."); //collapse repeated dots
  k = k.replace(/^\./, ''); // strip leading dot
  k = k.replace(/\.$/, ''); // strip ending dot
  k = k.trim();

  if (k == "") {
    return false;
  };

  var a = k.split(/\./);
  var last = a.pop();

  var prevO, prevN;
  while (a.length) {
    var n = a.shift();

    if (n.charAt(0) == "#") {
      isArrayAccess = true;
      n = n.substr(1);
      if (isNaN(parseInt(n))) {
        isArrayAccess = false;
        //remove quotes if any
        n = n.trim();
        n = n.replace(/^"/,'');
        n = n.replace(/"$/,'');
      }
    }

    if (!o) {
      o = (isArrayAccess)?[]:{};
      prevO[prevN] = o;
    }

    prevO = o;
    prevN = n;

    //cannot coerce an array into an object
    if (util.isArray(o) && !isArrayAccess) {
      return false;
    }

    if (!(n in o)) {
      o = undefined;
      continue;
    }

    o = o[n];
  }

  k = last;
  var isArrayAccess = (k.charAt(0) == "#");
  if (isArrayAccess) {
    k = k.substr(1);
  }

  if (!o) {
    o = (isArrayAccess)?[]:{};
    prevO[prevN] = o;
  }

  return handleWriteOps(op, o, k, v);
};


var get = function(o,k) {
  return readHandler("get", o, k);
};

var shift = function(o,k) {
  return readHandler("shift", o, k);
};

var pop = function(o,k) {
  return readHandler("pop", o, k);
};


var set = function(o, k, v) {
  return writeHandler("set", o, k, v);
};

var merge = function(o, k, v) {
  return writeHandler("merge", o, k, v);
};


var push = function(o, k, v) {
  return writeHandler("push", o, k, v);
};

var unshift = function(o, k, v) {
  return writeHandler("unshift", o, k, v);
};


module.exports.get = get;
module.exports.shift = shift;
module.exports.pop = pop;

module.exports.set = set;
module.exports.push = push;
module.exports.unshift = unshift;
module.exports.merge = merge;

module.exports.isMetaKey = isMetaKey;
module.exports.read = readHandler;
module.exports.write = writeHandler;