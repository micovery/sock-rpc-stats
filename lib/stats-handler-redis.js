var util = require("util");
var StatsHandlerMem = require("./stats-handler-mem.js");
var log = require("util-logging");
var logger =  new log.ConsoleLogger().setLevel(log.Level.INFO);
var redis = require("redis");
var async = require("async");
var url = require("url");


var StatsHandlerRedis = function() {
  var self = this;
  (self.super_ = StatsHandlerRedis.super_).call(self);
  return self;
};

util.inherits(StatsHandlerRedis, StatsHandlerMem);

StatsHandlerRedis.prototype.logErrors = function(err) {
  if (err && util.isError(err)) {
    logger.severe(err);
  }
};

StatsHandlerRedis.prototype.cleanup = function(err) {
  var self = this;
  var client = self.getClient();
  //close the connection if there was an error
  if (client && util.isError(err)){
    client.shutdown();
  }
  self.logErrors(err);
};

var trimChar = function(text, char) {
  if (!text) {
    return "";
  }
  text = text.replace(new RegExp("^"+ char + "+"),"");
  text = text.replace(new RegExp(""+ char +"+$"), "");
  return text;
};

StatsHandlerRedis.prototype.parseUrl = function(str) {

  try {
    var self = this;
    var prefix = "redis://";
    self.validateUrlPrefix(prefix, str);

    var data = url.parse(str, true);
    var options = {};

    if (!data.pathname) {
      throw new Error("No database number provided in url")
    }

    if (!data.hostname) {
      throw new Error("No host provided in url");
    }

    options.host = data.hostname.trim();
    options.database = parseInt(trimChar(data.pathname, "\\/").split("/")[0], 10);

    if (isNaN(options.database)) {
      throw new Error("Database must be a number between 0, and 15 (inclusive)")
    }

    if (data.auth) {
      var authParts = data.auth.split(":");
      if (authParts.length < 2) {
        throw new Error("No password in url");
      }

      options.username = authParts[0];
      options.password = authParts[1];
    }

    options.extraOptions =  {
      auth_pass: true,
      socket_keepalive: true,
      retry_max_delay: 60 * 1000
    };

    options.port = data.port || 6379;

    return options;
  }
  catch(ex) {
    logger.severe("Could not parse url: " + str);
    throw ex;
  }


};

StatsHandlerRedis.prototype.init = function(options, callback) {
  var self = this;
  options = options || {};
  callback = callback || function () {};

  try {

    if (typeof options.container !== "string") {
      throw new Error("--container argument is required. See usage with -h.");
    }

    self.setContainer(options.container);

    var clientOptions =  self.parseUrl(options.url);

    var connections = 0;
    var connectToDB = function (cb) {
      try {
        var client = new redis.createClient(
            clientOptions.port,
            clientOptions.host,
            clientOptions.extraOptions
        );

        self.setClient(client);

        client.on("error", function(err) {
          logger.severe(err);
          cb(err);
        });

        if (clientOptions.password) {
          client.auth(clientOptions.password);
        }

        client.on("ready", function() {
          if (connections++ > 0) {
            return client.select(clientOptions.database);
          }
          return client.select(clientOptions.database, cb);
        });
      }
      catch (ex) {
        cb(ex);
      }
    };

    async.waterfall(
        [
          connectToDB,
          function () {
            self.baseInit(options, self.toArray(arguments).pop());
          }
        ],
        function (ex) {
          callback(ex);
        }
    );
  }
  catch(ex) {
    callback(ex);
  }
};



StatsHandlerRedis.prototype.loadScopeData = function(scope, callback) {
  try {
    var self = this;

    callback = callback || function() {};

    var client = self.getClient();
    var container = self.getContainer();

    client.hgetall(container +":" + scope, function(err, result) {
      try {
        if (err || !result) {
          return callback(err, result);
        }

        var scopeData = self.unflatten(scope, result);
        return callback(null, scopeData);
      }
      catch (ex) {
        callback(ex);
      }
    });
  }
  catch (ex) {
    callback(ex);
  }
};

StatsHandlerRedis.prototype.logErrors = function(err) {
  try {
    if (err && util.isArray(err)) {
      err.forEach(function(item) {
        logger.severe(item);
      })
    }
  }
  catch(ex) {
    logger.severe(ex);
  }
};

StatsHandlerRedis.prototype.saveScopeData = function(scope, scopeData, callback) {
  try {
    var self = this;
    var args = self.toArray(arguments);
    callback = self.sequence(self.logErrors, self.defaultCallback(args).pop());

    var client = self.getClient();
    var container = self.getContainer();

    var hashKey = container + ":" + scope;
    var flatJson = self.flatten(scope, scopeData);

    client.multi()
    .del(hashKey)
    .hmset(hashKey, flatJson)
    .exec(function(err) {
      return callback(err);
    });
  }
  catch (ex) {
    callback(ex);
  }
};



module.exports = StatsHandlerRedis;