var SockRPCClient = require("sock-rpc").Client,
    util = require("util");


var Client = function(options) {
  var self = this;
  (self.super_ = Client.super_).call(self, options);
  return self;
};


util.inherits(Client, SockRPCClient);


Client.prototype.handle = function(op, args) {
  var self = this;
  var args = Array.prototype.slice.call(args, 0);
  var cb = self.defaultCallback(args).pop();
  args = [op, args,
    function(err, packet) {
      try {
        if (err) throw err;

        var result = JSON.parse(packet);

        if (!util.isArray(result)) {
          throw new Error("protocol error: expected result to be an array, but got: " + JSON.stringify(result));
        }

        var error = result[0];
        var data = result[1];

        if (error) {
          throw new Error(data);
        }

        cb(undefined, data);
      }
      catch(ex) {
        cb(ex);
      }
    }
  ];
  self.sock_rpc.apply(self, args);
};

Client.prototype.merge = function() {
  this.handle("merge", arguments);
};

Client.prototype.pop = function() {
  this.handle("pop", arguments);
};

Client.prototype.push = function() {
  this.handle("push", arguments);
};

Client.prototype.shift = function() {
  this.handle("shift", arguments);
};

Client.prototype.unshift = function() {
  this.handle("unshift", arguments);
};

Client.prototype.count = function() {
  this.handle("count", arguments);
};

Client.prototype.keys = function() {
  this.handle("keys", arguments);
};

Client.prototype.set = function() {
  this.handle("set", arguments);
};

Client.prototype.get = function() {
  this.handle("get", arguments);
};

Client.prototype.wipe = function() {
  this.handle("wipe", arguments);
};

Client.prototype.flush = function() {
  this.handle("flush", arguments);
};


module.exports = Client;

