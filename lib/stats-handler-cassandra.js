var util = require("util"),
    StatsHandlerMem = require("./stats-handler-mem.js"),
    log = require("util-logging"),
    logger =  new log.ConsoleLogger().setLevel(log.Level.INFO),
    cassandra = require("cassandra-driver"),
    async = require("async"),
    url = require("url");


var StatsHandlerCassandra = function() {
  var self = this;
  (self.super_ = StatsHandlerCassandra.super_).call(self);
  return self;
};

util.inherits(StatsHandlerCassandra, StatsHandlerMem);


StatsHandlerCassandra.prototype.logErrors = function(err) {
  if (err && err.innerErrors) {

    if (util.isArray(err.innerErrors))  {
      err.innerErrors.forEach(function (inner) {
        logger.severe(inner);
      });
    }
    else if (typeof err.innerErrors == "object") {
      for (var key in err.innerErrors) {
        if (err.innerErrors.hasOwnProperty(key)) {
          logger.severe(err.innerErrors[key]);
        }
      }
    }
    else {
      logger.severe(err.innerErrors);
    }
  }
};

StatsHandlerCassandra.prototype.cleanup = function(err) {
  var self = this;
  var client = self.getClient();
  //close the connection if there was an error
  if (client && util.isError(err)){
    client.shutdown();
  }
  self.logErrors(err);
};

var trimChar = function(text, char) {
  if (!text) {
    return "";
  }
  text = text.replace(new RegExp("^"+ char + "+"),"");
  text = text.replace(new RegExp(""+ char +"+$"), "");
  return text;
};

StatsHandlerCassandra.prototype.parseUrl = function(str) {
  try {
    var self = this;
    var prefix = "cassandra://";
    self.validateUrlPrefix(prefix, str);

    var data = url.parse(str, true);
    var options = {};

    if (!data.pathname) {
      throw new Error("No keySpace provided in url")
    }

    if (!data.hostname) {
      throw new Error("No host provided in url string");
    }

    options.keyspace = trimChar(data.pathname, "\\/").split("/")[0];
    options.contactPoints = trimChar(data.hostname, ",").split(",");

    if (data.auth) {
      var authParts = data.auth.split(":");
      if (authParts.length < 2) {
        throw new Error("No password in url");
      }
      options.authProvider = new cassandra.auth.PlainTextAuthProvider(authParts[0], authParts[1]);
    }

    options.protocolOptions = {};
    options.protocolOptions.port = data.port || 9042;

    return options;
  }
  catch(ex) {
    logger.severe("Could not parse url parameter: " + str);
    throw ex;
  }
};

StatsHandlerCassandra.prototype.init = function(options, callback) {
  var self = this;
  options = options || {};
  callback = callback || function () {};


  var callback = sequence(function(err) {self.cleanup(err)}, callback);

  try {

    self.setContainer(options.container);

    var clientOptions =  self.parseUrl(options.url);

    var connectToDB = function (cb) {
      try {
        var client = new cassandra.Client(clientOptions);
        client.connect(
            function (err) {
              try {
                if (err) {
                  logger.severe("could not connect to: " + options.url);
                  throw err;
                }
                self.setClient(client);
                return cb(null);
              }
              catch (ex) {
                cb(ex);
              }
            }
        );
      }
      catch (ex) {
        cb(ex);
      }
    };

    var createTable = function(cb) {
      try {
        var client = self.getClient();

        var createStmt =
            " create table if not exists " + self.getContainer()  +
            "(" +
            "   scope varchar," +
            "   key varchar," +
            "   value varchar," +
            "   primary key(scope, key)" +
            ");";

        client.execute(createStmt, function (err) {
          if (err) {
            logger.severe("Could not create table: " + self.getContainer());
          }
          return cb(err);
        });

      }
      catch (ex) {
        cb(ex);
      }
    };

    async.waterfall(
        [
          connectToDB,
          createTable,
          function() {
            self.baseInit(options, self.toArray(arguments).pop());
          }
        ],
        function(ex) {
          callback(ex);
        }
    );
  }
  catch(ex) {
    callback(ex);
  }
};

var sequence = function(func1, func2) {
  return function() {
    var args = Array.prototype.slice.call(arguments, 0);
    func1.apply(this, args);
    func2.apply(this, args);
  }
};

StatsHandlerCassandra.prototype.loadScopeData = function(scope, callback) {
  try {
    var self = this;
    callback = callback || function() {};
    var callback = sequence(function(err) {self.logErrors(err)}, callback);

    var client = self.getClient();
    var container = self.getContainer();

    var selectStmt = "select key, value from " + container + " where scope = ?;";
    client.execute(selectStmt, [scope], function(err, result) {
      try {
        if (err) {
          return callback(err);
        }

        var rows = result.rows;

        if (rows.length == 0) {
          return callback(null);
        }

        var flatJson = {};
        rows.forEach(function(row) {
          var key = row.key;
          var value = row.value;
          flatJson[key] = value;
        });

        var scopeData = self.unflatten(scope, flatJson);
        return callback(null, scopeData);
      }
      catch(ex) {
        callback(ex);
      }
    });
  }
  catch (ex) {
    callback(ex);
  }
};

StatsHandlerCassandra.prototype.saveScopeData = function(scope, scopeData, callback) {
  try {
    var self = this;
    callback = callback || function() {};
    var callback = sequence(function(err) {self.logErrors(err)}, callback);

    var client = self.getClient();
    var container = self.getContainer();

    //no transactions in Cassandra, so this is the best we can do ... oh well fuck it
    var deleteKeys = function(cb) {
      try {
        var stmt = "delete from scopes where scope = ?;";
        client.execute(stmt, [scope], function(err) {
          return cb(err);
        });
      }
      catch (ex) {
        cb(ex);
      }
    };

    var insertKeys =  function(cb) {
      try {
        var queries = [];
        var stmt = "insert into " + container + "(scope, key, value) values(?, ?, ?);";
        var flatJson = self.flatten(scope, scopeData);

        Object.keys(flatJson).forEach(function (key) {
          queries.push({
            "query": stmt,
            "params": [scope, key, flatJson[key]]
          });
        });

        client.batch(queries, {}, function (err, result) {
          return cb(err);
        });
      }
      catch(ex) {
        cb(ex);
      }
    };

    async.waterfall(
      [
        deleteKeys,
        insertKeys
      ],
      function(err) {
        callback(err);
      }
    );
  }
  catch (ex) {
    callback(ex);
  }
};


module.exports = StatsHandlerCassandra;