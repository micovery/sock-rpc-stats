require("./json-extras.js");

var util = require("util"),
    async = require("async"),
    log = require("util-logging"),
    logger =  new log.ConsoleLogger().setLevel(log.Level.INFO),
    crypto = require("crypto"),
    stringify  = require("json-stable-stringify"),
    dot = require("./dot.js");



var StatsHandlerMem = function() {
  var self =  this;
  StatsHandlerMem.super_.call(self);

  self.setData({});
  self.setInterval(60);
  self.setBackup(true);

  return self;
};

util.inherits(StatsHandlerMem, Object);

StatsHandlerMem.prototype.preUpdate = function(scopeData) {
  if (!scopeData) {
    return;
  }

  var cTime = new Date().getTime();
  if (!scopeData.createdAt_) {
    scopeData.createdAt_ = cTime;
  }
  scopeData.updatedAt_ = cTime;

  return scopeData;
};


StatsHandlerMem.prototype.postRead = function(scopeData) {
  var cTime = new Date().getTime();
  if (!scopeData.createdAt_) {
    scopeData.createdAt_ = cTime;
  }

  if (!scopeData.updatedAt_) {
    scopeData.updatedAt_ = cTime;
  }

  return scopeData;
};

StatsHandlerMem.prototype.flatten = function(scope, fatJson) {
  var flatJson = JSON.flatten(fatJson);
  var keys = Object.keys(flatJson);
  keys.forEach(function(key) {
    try {
      flatJson[key] = JSON.stringify(flatJson[key]);
    }
    catch (ex) {
      logger.severe("Error serializing value \"%s\" for key \"%s\" within scope \"%s\". %s", fatJson[key], key, scope, ex.toString());
    }
  });
  return flatJson;
};

StatsHandlerMem.prototype.md5 = function(str) {
  var md5sum = crypto.createHash('md5');
  return md5sum.update(str).digest("base64");
}


StatsHandlerMem.prototype.unflatten = function(scope, flatJson) {
  var keys = Object.keys(flatJson);
  var keys = Object.keys(flatJson);

  keys.forEach(function(key) {
    try {
      flatJson[key] = JSON.parse(flatJson[key]);
    }
    catch(ex) {
      logger.severe("Error parsing value \"%s\" for key \"%s\" within scope \"%s\". %s", fatJson[key], key, scope, ex.toString());
    }
  });

  var fatJson = JSON.unflatten(flatJson);
  return fatJson;
};

StatsHandlerMem.prototype.init = function(options, callback) {
  return this.baseInit(options, callback)
};

StatsHandlerMem.prototype.baseInit = function(options, callback) {
  try {
    var self = this;
    var options = options || {};

    options.interval = parseInt(options.interval || self.getInterval().toString(), 10);

    if (isNaN(options.interval) || options.interval <= 0) {
      throw new Error("The persist interval value must an integer greater than zero");
    };


    self.setInterval(options.interval);
    options.backup = String(options.backup).toLowerCase();
    self.setBackup(options.backup == "true" || options.backup == "1");
    self.initInterval();
    callback(null);
  }
  catch(ex) {
    callback(ex);
  }
};

StatsHandlerMem.prototype.setInterval = function(interval) {
  this._interval = interval;
};

StatsHandlerMem.prototype.getInterval = function() {
  return this._interval;
};

StatsHandlerMem.prototype.setBackup = function(backup) {
  this._backup = backup;
};

StatsHandlerMem.prototype.getBackup = function() {
  return this._backup;
};

StatsHandlerMem.prototype.setScopeData = function(scope, scopeData) {
  this.getData()[scope] = scopeData;
};

StatsHandlerMem.prototype.getScopeData = function(scope) {
  return this.getData()[scope];
};

StatsHandlerMem.prototype.stopInterval =  function() {
  var self = this;
  if (!self._interval_started) {
    //nothing to do, interval has not started
    return;
  }

  clearInterval(self._interval_id);
  delete self._interval_id;
  self._interval_started = false;
  return;
};

StatsHandlerMem.prototype.initInterval =  function() {
  try {
    var self = this;

    if (self._interval_started) {
      logger.info("Interval loop already active, will not start again");
      logger.finer(new Error());
      return;
    }
    self._interval_started = true;

    logger.fine("Initializing interval timer");

    var trigger = function() {
      self._interval_id = setTimeout(handler, self.getInterval() * 1000);
    };

    var goAgain = function(previousId){
      if (previousId == undefined ||
          self._interval_id == undefined ||
          self._interval_id !== previousId) {
        logger.info("Interval timer stopped, previous ID did not match ...")
        return;
      }
      trigger();
    };

    var handler = function() {
      var interval_id = self._interval_id;
      try {
        self.saveData(function(ex, records) {
          if (ex) {
            logger.severe(ex);
          }
          logger.fine("scopes saved: " + records);
          return goAgain(interval_id);
        });
      }
      catch(ex) {
        logger.severe(ex);
        return goAgain(interval_id);
      }
    };

    trigger();
  }
  catch (ex) {
    cb(ex);
  }
};

StatsHandlerMem.prototype.setData = function(data) {
  this.data = data;
};

StatsHandlerMem.prototype.getData = function() {
  return this.data;
};


StatsHandlerMem.prototype._backupScopeData = function(scope, scopeData, callback) {
  try {
    var self = this;
    var cTime = new Date().getTime();
    var backupScope = scope + "." + cTime + ".bak"
    self._saveScopeData(backupScope, scopeData, function(err) {
      if (!err) {
        scopeData.backups_ = scopeData.backups_ || {};
        scopeData.backups_[cTime] = backupScope;
      }
      return callback(err);
    });
  }
  catch (ex) {
    callback(ex);
  }
};

StatsHandlerMem.prototype._loadScopeData = function(scope, callback) {
  try {
    var self = this;
    if (typeof scope !== "string") {
      throw new Error("scope argument is required");
    }

    var scopeData = self.getScopeData(scope);

    if (scopeData) {
      return callback(null, scopeData);
    }

    return self.loadScopeData(scope, function(err, scopeData) {
      try {
        if (err) {
          throw err;
        }

        if (!scopeData) {
          return callback(null);
        };

        //optionally, backup the scope data when it is read from the database
        if (self.getBackup()) {
          self._backupScopeData(scope, scopeData, function (err) {
            return callback(err, scopeData);
          });
          return;
        };

        return callback(err, scopeData);
      }
      catch (ex) {
        callback(ex);
      }
    });
  }
  catch(ex) {
    callback(ex);
  }
};

StatsHandlerMem.prototype.loadScopeData = function(scope, callback) {
  try {
    var self = this;
    var scopeData = self.getScopeData(scope);
    return callback(null, scopeData);
  }
  catch(ex) {
    callback(ex);
  }
};

/**
 * Check if the object has been modified or not.
 * It uses an internal crypto signature to check if the object has
 * changed since it was last signed.
 *
 * Internal properties such as createdAt_, and updatedAt_ are ignored when
 * computing the signature.
 *
 * @param scopeData
 * @private
 */
StatsHandlerMem.prototype._wasModified = function(scopeData) {
  var self = this;

  var oldSignature = scopeData.signature_;

  var metaData = {};

  //remove metadata before computing hash
  for (var field in scopeData) {
    if (self.isMetaKey(field) && scopeData.hasOwnProperty(field)) {
      if (field == "backups_") {
        //we want to include backups as part of the signature
        continue;
      }
      metaData[field] = scopeData[field];
      scopeData[field] = undefined;
    }
  }

  //compute the new signature
  var newSignature = self.md5(stringify(scopeData));  //use a stable stringify for hashing

  //restore the values for metadata
  for (var field in metaData) {
    if (metaData.hasOwnProperty(field)) {
      scopeData[field] = metaData[field];
    }
  }

  if (oldSignature != newSignature) {
    scopeData.signature_ = newSignature;
    return true;
  }

  return false;
};

StatsHandlerMem.prototype._saveScopeData = function(scope, scopeData, callback) {
  try {
    var self = this;

    return self.saveScopeData(scope, scopeData, callback);
  }
  catch(ex) {
    callback(ex);
  }
};

StatsHandlerMem.prototype._saveScope = function(scope, callback) {
  try {
    var self = this;

    if (typeof scope !== "string") {
      throw new Error("scope argument is required");
    }

    var scopeData = self.getData()[scope];

    if (!scopeData) {
      throw new Error("scope " + scope + " is not active");
    }

    return self.saveScopeData(scope, scopeData, callback);
  }
  catch(ex) {
    callback(ex);
  }

};

StatsHandlerMem.prototype.saveScopeData = function(scope, scopeData, callback) {
  try {
    return callback(null, scopeData);
  }
  catch(ex) {
    callback(ex);
  }
};

StatsHandlerMem.prototype.saveData = function(scope, callback) {

  var self = this;
  var args = self.toArray(arguments);
  var callback = self.defaultCallback(args).pop();
  var scopes = args;

  var mainCb = function(ex) {
    callback(ex, records);
  };

  try {
    var self = this;
    var data = self.getData();

    //if no scopes specified, save all
    if (scopes.length == 0) {
      scopes = Object.keys(data);
    }

    var records = 0;

    async.eachSeries(
        scopes,
        function(scope, nextCb) {
          try {
            //ignore non-strings
            if (typeof scope !== "string") {
              return nextCb(null);
            }

            var scopeData = data[scope];

            //skip the scope if it does not exist
            if (!scopeData) {
              return nextCb(null);
            }

            if (!self._wasModified(scopeData)) {
              logger.fine("Not saving scope \"" + scope + "\", has not changed.");
              return nextCb(null);
            }

            logger.fine("Saving scope \"" + scope + "\"");

            return self._saveScopeData(scope, scopeData, function(ex){
              //error happened, clear the signature, to force a save next time
              ex && delete scopeData.signature_;
              //no error, increment the updated records count
              ex || records++;
              nextCb(ex);
            });
          }
          catch(ex) {
            nextCb(ex);
          }
        },
        mainCb
    );
  }
  catch(ex) {
    mainCb(ex);
  }
};

StatsHandlerMem.prototype.isValidKey = function(key) {
  return typeof key == "string" && !this.isMetaKey(key);
};

StatsHandlerMem.prototype.isMetaKey = function(key) {
  return dot.isMetaKey(key);
};

StatsHandlerMem.prototype.writeKey = function (operation, scope, key, value, callback) {
  var self = this;
  var callback = self.defaultCallback(self.toArray(arguments)).pop();

  if (!self.isValidKey(key)) {
    return callback(new Error(util.format("key name %s not allowed", key)));
  }

  self.writeKeys(operation, scope, [key, value], callback);
};

StatsHandlerMem.prototype.readKey = function(operation, scope, key, _default, callback) {
  var self = this;
  var args = self.toArray(arguments);
  var callback = self.defaultCallback(args).pop();

  operation = args.shift();
  scope = args.shift();
  key = args.shift();
  _default = args.shift();


  if (!self.isValidKey(key)) {
    return callback(null);
  }

  var cb = function (ex, result) {
    try {
      if (ex) {
        throw ex;
      }

      if (!result) {
        return callback(undefined, null);
      }

      return callback(undefined, result[key]);
    }
    catch (ex) {
      callback(ex);
    }
  };

  self.readKeys(operation, scope, [key, _default], cb);
};



StatsHandlerMem.prototype.readKeys = function(operation, scope, pairs, callback) {
  try {
    var self = this;
    var args = self.toArray(arguments);

    var callback = self.defaultCallback(args).pop();
    var operation = args.shift();
    var scope = args.shift();
    var pairs = args;

    if (!scope) {
      throw new Error("scope argument is required");
    }

    var outData = self.pairsToMap(pairs);

    var handleReadKeys = function(scopeData, cb) {
      try {
        var args = Array.prototype.slice.call(arguments, 0);
        cb = args.pop();
        scopeData = args.shift();

        var scopeData = self.postRead(scopeData || {}) ;

        self.setScopeData(scope, scopeData);
        outData = self.safeObjectShallowCopy(operation, scopeData, outData);

        return cb(null, outData);
      }
      catch (ex) {
        cb(ex);
      }
    };

    async.waterfall(
        [
          function(cb) {self._loadScopeData(scope, cb); },
          handleReadKeys
        ],
        function(ex, result) {
          callback(ex, result);
        }
    );
  }
  catch (ex) {
    callback(ex);
  }
};


StatsHandlerMem.prototype.pairsToMap = function(pairs) {
  var self = this;
  var len = pairs.length;
  var map = {};
  while (len--) {
    var pair = pairs[len];
    if (!util.isArray(pair) || (pair.length < 1 || pair.length > 2)) {
      continue;
    }
    map[pair[0]] = pair[1];
  }

  return map;
};

StatsHandlerMem.prototype.deleteInvalidKeys = function(map) {
  if (!map || typeof map !== "object") {
    return;
  }

  var self = this;

  for (var key in map) {
    if (map.hasOwnProperty(key)) {
      self.isValidKey(key) || delete map[key];
    }
  }

};

StatsHandlerMem.prototype.flushScopes = function(scope, callback) {
  var self = this;
  var args = self.toArray(arguments);
  var callback = self.defaultCallback(args).pop();

  try {
    args.push(function(err, result) {
      try {
        if (err) {
          throw err;
        }
        args.pop(); //remove the callback
        var data = self.getData();
        args.forEach(function (scope) {
          data[scope] && delete data[scope];
        });
        return callback(err, result);
      }
      catch(ex) {
        callback(ex);
      }
    });
    self.saveData.apply(self, args);
  }
  catch(ex) {
    callback(ex);
  }
};

var setProperty = function(container, prop, value) {
  dot.set(container, prop, value);
};



StatsHandlerMem.prototype.wipeScope = function(scope, callback) {
  var self = this;
  var args = self.toArray(arguments);
  var callback = self.defaultCallback(args).pop();
  scope = args.shift();

  try {
    if (typeof scope !== "string") {
      throw new Error("scope argument required");
    }

    var handleWipeKeys = function(scopeData, cb) {
      try {
        var args = Array.prototype.slice.call(arguments, 0);
        cb = args.pop();
        scopeData = args.shift();

        scopeData = self.postRead(scopeData || {});

        self.preUpdate(scopeData);
        self.setScopeData(scope, scopeData);

        for (var key in scopeData) {
          if (scopeData.hasOwnProperty(key) && !self.isMetaKey(key)) {
            delete scopeData[key];
          }
        }

        logger.fine(scopeData);

        return callback(null, true);
      }
      catch (ex) {
        cb(ex);
      }
    };

    async.waterfall(
      [
        function(cb) {self._loadScopeData(scope, cb); },
        handleWipeKeys
      ],
      function(ex, result) {
        callback(ex, result);
      }
    );
  }
  catch (ex) {
    callback(ex);
  }
};

StatsHandlerMem.prototype.writeKeys = function(operation, scope, pairs, callback) {
  var self = this;
  var args = self.toArray(arguments);
  var callback = self.defaultCallback(args).pop();

  operation = args.shift();
  scope = args.shift();
  pairs = args;

  try {
    if (typeof scope !== "string") {
      throw new Error("scope argument required");
    }

    var dataMap = self.pairsToMap(pairs);
    self.deleteInvalidKeys(dataMap);

    var handleWriteKeys = function(scopeData, cb) {
      try {
        var args = Array.prototype.slice.call(arguments, 0);
        cb = args.pop();
        scopeData = args.shift();

        scopeData = self.postRead(scopeData || {});

        self.preUpdate(scopeData);
        self.setScopeData(scope, scopeData);

        for (var key in dataMap) {
          if (dataMap.hasOwnProperty(key)) {
            var value = dataMap[key];
            dot.write(operation, scopeData, key, value);
          }
        }

        logger.fine(scopeData);

        return callback(null, true);
      }
      catch (ex) {
        cb(ex);
      }
    };

    async.waterfall(
        [
          function(cb) {self._loadScopeData(scope, cb); },
          handleWriteKeys
        ],
        function(ex, result) {
          callback(ex, result);
        }
    );
  }
  catch (ex) {
    callback(ex);
  }
};

StatsHandlerMem.prototype.defaultCallback = function(args, cb) {
  if (!cb || typeof cb !== "function") {
    cb = function() {};
  }

  if (!args || !util.isArray(args)) {
    return [cb];
  }

  if (typeof args[args.length -1]  !== "function") {
    args.push(cb);
  }

  return args;
};

StatsHandlerMem.prototype.read = function() {
  var self = this;
  var args = self.toArray(arguments);
  var operation = args.shift();
  var callback = self.defaultCallback(args).pop();

  try {

    //keys(), get()
    if (["keys", "get"].indexOf(operation) >= 0 && args.length == 0) {
      var keys = Object.keys(self.getData() || {});
      callback(undefined, keys);
      return;
    }

    //get(scope, callback)
    if (operation == "get" && args.length == 1 && typeof args[0] == "string") {
      args.unshift(operation);
      args.push(callback);

      self.readKeys.apply(self, args);
      return;
    }

    //count(scope, callback), keys(scope, callback
    if (["count", "keys"].indexOf(operation) >= 0 && args.length == 1 && typeof args[0] == "string") {
      args.unshift(operation);
      args.push(".");
      args.push(callback);

      self.readKey.apply(self, args);
      return;
    }

    //get(scope, key, default, callback)
    if (args.length >= 2 && typeof args[0] == "string" && typeof args[1] == "string") {
      args.unshift(operation);
      args.push(callback);

      self.readKey.apply(self, args);
      return;
    }

    //get(scope, [key,default], [key,default],..., callback)
    if (args.length >= 2 && typeof args[0] == "string" && util.isArray(args[1])) {
      args.unshift(operation);
      args.push(callback);

      self.readKeys.apply(self, args);
      return;
    }

    throw new Error("Unknown "+operation+"() function.");
  }
  catch(ex) {
    callback(ex);
  }
};

StatsHandlerMem.prototype.get = function() {
  var self = this;
  var args = self.toArray(arguments);
  args.unshift("get");
  return self.read.apply(this, args);
};

StatsHandlerMem.prototype.pop = function() {
  var self = this;
  var args = self.toArray(arguments);
  args.unshift("pop");
  return self.read.apply(this, args);
};

StatsHandlerMem.prototype.shift = function() {
  var self = this;
  var args = self.toArray(arguments);
  args.unshift("shift");
  return self.read.apply(this, args);
};

StatsHandlerMem.prototype.count = function() {
  var self = this;
  var args = self.toArray(arguments);
  args.unshift("count");
  return self.read.apply(this, args);
};

StatsHandlerMem.prototype.keys = function() {
  var self = this;
  var args = self.toArray(arguments);
  args.unshift("keys");
  return self.read.apply(this, args);
};


StatsHandlerMem.prototype.flush = function() {
  var self = this;
  var args = self.toArray(arguments);
  var callback = self.defaultCallback(args).pop();

  try {
    //flush(scope, ..., callback)
    if (args.length >= 1 && typeof args[0] == "string") {
      args.push(function(err, result) {
        callback(err, result);
      });

      self.flushScopes.apply(self, args);
      return;
    }
    throw new Error("Unknown flush() function.");
  }
  catch(ex) {
    callback(ex);
  }
};

StatsHandlerMem.prototype.wipe = function() {
  var self = this;
  var args = self.toArray(arguments);
  var callback = self.defaultCallback(args).pop();

  try {
    //wipe(scope)
    if (args.length == 1 && typeof args[0] == "string") {
      args.push(function(err, result) {
        callback(err, result);
      });

      self.wipeScope.apply(self, args);
      return;
    }
    throw new Error("Unknown wipe() function.");
  }
  catch(ex) {
    callback(ex);
  }
};

StatsHandlerMem.prototype.meta = function() {
  var self = this;
  var args = self.toArray(arguments);
  var callback = self.defaultCallback(args).pop();
  var scope = args.shift();

  try {
    if (!scope || typeof scope != "string")  {
      throw new Error("scope argument is required");
    }


    var scopeData = self.getScopeData(scope);
    if (!scopeData) {
      return callback(null,null);
    };

    var result = {};
    var keys = Object.keys(scopeData);
    keys.forEach(function(key,index) {
      if (scopeData.hasOwnProperty(key) && self.isMetaKey(key)) {
        //make a copy of the data to so that original can't be modified
        result[key] = JSON.parse(JSON.stringify(scopeData[key]));
      }
    });

    return callback(null, result);
  }
  catch(ex) {
    callback(ex);
  }
};


StatsHandlerMem.prototype.write = function() {
  var self = this;
  var args = self.toArray(arguments);
  var operation = args.shift();
  var callback = self.defaultCallback(args).pop();

  try {

    //set(scope, key, value, callback)
    if (args.length == 3 && typeof args[0] == "string" && typeof args[1] == "string") {
      args.unshift(operation);
      args.push(callback);

      self.writeKey.apply(self, args);
      return;
    }

    //set(scope, pair1, ..., callback)
    if (args.length >= 2 && typeof args[0] == "string" && util.isArray(args[1])) {
      args.unshift(operation);
      args.push(callback);

      self.writeKeys.apply(self, args);
      return;
    }

    throw new Error("Unknown "+operation+"() function.");
  }
  catch(ex) {
    callback(ex);
  }
};

StatsHandlerMem.prototype.set = function() {
  var self = this;
  var args = self.toArray(arguments);
  args.unshift("set");
  self.write.apply(this, args);
};

StatsHandlerMem.prototype.push = function() {
  var self = this;
  var args = self.toArray(arguments);
  args.unshift("push");
  self.write.apply(this, args);
};

StatsHandlerMem.prototype.unshift = function() {
  var self = this;
  var args = self.toArray(arguments);
  args.unshift("unshift");
  self.write.apply(this, args);
};

StatsHandlerMem.prototype.merge = function() {
  var self = this;
  var args = self.toArray(arguments);
  args.unshift("merge");
  self.write.apply(this, args);
};


StatsHandlerMem.prototype._getKeyMap = function(pairs) {
  if (!util.isArray(pairs)) {
    return {};
  }

  var keyMap = {};
  pairs.forEach(function(pair) {
    if (util.isArray(pair) && pair[0]) {
      keyMap[pair[0]] = pair[1];
    }
  });

  return keyMap;
};



StatsHandlerMem.prototype.setClient = function(client) {
  this._client = client;
};

StatsHandlerMem.prototype.getClient = function() {
  return this._client;
};

StatsHandlerMem.prototype.setContainer = function(container) {
  if (typeof container !== "string") {
    throw new Error("--container argument is required. See usage with -h.");
  }
  this._container = container;
};

StatsHandlerMem.prototype.getContainer = function() {
  return this._container;
};

StatsHandlerMem.prototype.toArray = function(args){
  return Array.prototype.slice.call(args, 0);
};

StatsHandlerMem.prototype.validateUrlPrefix = function(prefix, url) {
  if (!url || typeof url !== "string") {
    throw new Error("--url argument is required. See usage with -h.");
  }

  if (!prefix) {
    throw new Error("prefix argument is required");
  }

  url = url.trim();

  if (typeof prefix == "string") {
    if (url.indexOf(prefix) !== 0) {
      throw new Error("value of the --url parameter does not start with: " + prefix);
    }
    return;
  }

  if (util.isArray(prefix)) {
    var valid = false;
    prefix.forEach(function(item) {
      if (url.indexOf(item) == 0) {
        valid = true;
      }
    });

    if (!valid) {
      throw new Error("value of the --url parameter does not start with any of: " + JSON.stringify(prefix));
    }
    return;
  }
};

/**
 * Fills the target object with the values form the source object.
 *
 * If the target object is empty, all keys are copied from source to target
 * If the target object already has some keys, only those keys are copied
 *
 * If a key is present in the target object, but not in the source object, it is left unchanged.
 *
 * @param source
 * @param target
 * @returns {*}
 */
StatsHandlerMem.prototype.safeObjectShallowCopy = function(operation, source, target) {
  var self = this;
  var keySet = Object.keys(target);
  if (keySet.length == 0) {
    keySet = Object.keys(source);
  }

  keySet.forEach(function(key) {
    var sourceValue = dot.read(operation, source, key);
    var targetValue = target[key];
    if (!(targetValue == undefined || targetValue == null)  &&
       (sourceValue == undefined || sourceValue == null)) {
      return;
    }

    target[key] = sourceValue;
  });

  self.deleteInvalidKeys(target);

  return target;
};

StatsHandlerMem.prototype.sequence = function(func1, func2) {
  var funcs = Array.prototype.slice.call(arguments, 0);
  return function() {
    var args = Array.prototype.slice.call(arguments, 0);
    var self = this;
    funcs.forEach(function(func) {
      func.apply(self, args);
    });
  };
};

module.exports = StatsHandlerMem;