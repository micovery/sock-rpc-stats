#!/usr/bin/env node
"use strict";
var rpc = require("sock-rpc");
var log = require("util-logging");
var wait = require("wait.for-es6");
var repl = require("repl");
var util = require("util");
var extras = require("./repl-extras.js");

var url2lib = function(url) {
  if (typeof url !== "string") {
    throw new Error("--url argument is required. See usage with --help");
  }

  url = url.trim();
  var lib = null;

  var map = module.exports.protocolMap;
  for (var key in map) {
    if (map.hasOwnProperty(key) && url.indexOf(key) == 0) {
      return map[key];
    }
  };

  throw new Error("Unknown protocol for --url: " + url);
};

module.exports = function(argv) {
  var lib = url2lib(argv.url);
  var StatsHandler = require(lib);
  var logger = new log.ConsoleLogger().setLevel(log.Level.INFO);

  var stats = new StatsHandler();

  rpc.register("set", function() {stats.set.apply(stats, arguments);});
  rpc.register("get", function() {stats.get.apply(stats, arguments);});
  rpc.register("push", function() {stats.push.apply(stats, arguments);});
  rpc.register("pop", function() {stats.pop.apply(stats, arguments);});
  rpc.register("shift", function() {stats.shift.apply(stats, arguments);});
  rpc.register("unshift", function() {stats.unshift.apply(stats, arguments);});
  rpc.register("merge", function() {stats.merge.apply(stats, arguments);});
  rpc.register("count", function() {stats.count.apply(stats, arguments);});
  rpc.register("keys", function() {stats.keys.apply(stats, arguments);});
  rpc.register("flush", function() {stats.flush.apply(stats, arguments);});
  rpc.register("wipe", function() {stats.wipe.apply(stats, arguments);});


  var run = function() {
    stats.init(
        argv,
        function (err) {
          if (err) {
            logger.severe(err);
            return;
          }
          rpc.listen();

          argv["repl"] && initREPL(stats);
        }
    );
  };
  run();
};

var initREPL = function(stats) {
  setTimeout(function() {
    extras.extend(repl.start("> "), stats);
  }, 2);
};



module.exports.usage =
    "\n" +
    "Usage:\n sock-rpc-stats --url=<string> [--container=<string>] [-i=<interval>]  [--host=<listen_host>] [--port=<listen_port>]\n" +
    "\n" +
    "Supported storage systems, and connection URL syntax:\n" +
    "\n" +
    "  * File System                 --url=file://path/to/directory\n" +
    "  * MySQL       (>= 5.5.38)     --url=mysql://user:pass@host:port/database \n" +
    "  * Cassandra   (>= 1.2)        --url=cassandra://user:pass@host1,host2,etc:port/keySpace\n"  +
    "  * CouchDB     (>= 1.5.0)      --url=http://user:pass@host:port/database\n" +
    "  * Redis       (>= 2.8.4)      --url=redis://:pass@host:port/database\n" +
    "\n" +
    "--container, -c:\n" +
    "\n" +
    "    For File System, this is ignored.\n" +
    "    For MySQL, this is the name used for the SQL table.\n" +
    "    For Cassandra, this is the name used for the CQL3 table.\n" +
    "    For CouchDB, this is ignored\n" +
    "    For Redis, this the prefix used for the hash keys";



module.exports.options = {
  url: {
    alias: "u",
    describe: "Connection URL",
    required: true
  },
  container: {
    alias: "c",
    default: "scopes",
    describe: "Container name"
  },
  backup: {
    alias: "b",
    default: true,
    type: 'boolean',
    describe: "backup data when reading it from database"
  },
  interval: {
    alias: "i",
    default: 60,
    describe: "interval in seconds between database saves"
  },
  repl: {
    default: false,
    describe: "enable Node.js Read-Eval-Print-Loop(REPL)"
  }
};

for (var key in rpc.options) {
  if (rpc.options.hasOwnProperty(key)) {
    module.exports.options[key] = rpc.options[key];
  }
}

module.exports.protocolMap =  {
  "cassandra://": "./stats-handler-cassandra.js",
  "file://": "./stats-handler-fs.js",
  "mysql://": "./stats-handler-mysql.js",
  "redis://": "./stats-handler-redis.js",
  "http://": "./stats-handler-couch.js",
  "https://": "./stats-handler-couch.js"
};



