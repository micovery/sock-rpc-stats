var util = require("util");
var StatsHandlerMem = require("./stats-handler-mem.js");
var log = require("util-logging");
var logger =  new log.ConsoleLogger().setLevel(log.Level.INFO);
var mysql = require("mysql");
var async = require("async");



var StatsHandlerMySQL = function() {
  var self = this;
  (self.super_ = StatsHandlerMySQL.super_).call(self);
  return self;
};

util.inherits(StatsHandlerMySQL, StatsHandlerMem);


StatsHandlerMySQL.prototype.cleanup = function(err) {
  var conn = this.getClient();
  //close the connection if there was an error
  if (conn && util.isError(err)){
    conn.destroy();
  }
};


StatsHandlerMySQL.prototype.parseUrl = function(str) {
  try {
    var self = this;

    var prefix = "mysql://";
    self.validateUrlPrefix(prefix, str);

    str = str.replace(prefix, "");

    var options = {};
    options.directory = str;

    return options;
  }
  catch(ex) {
    logger.severe("Could not parse url: " + str);
    throw ex;
  }
};

StatsHandlerMySQL.prototype.init = function(options, callback) {
  var self = this;
  var args = self.toArray(arguments);
  var callback = self.sequence(function(err) {self.cleanup(err)}, self.defaultCallback(args).pop());

  options = args.shift() || {};

  try {
    if (typeof options.container !== "string") {
      throw new Error("--container argument is required. See usage with -h.");
    }

    if (typeof options.url !== "string") {
      throw new Error("--url argument is required. See usage with -h.");
    }

    self.setContainer(mysql.escapeId(options.container));

    var connectToDB = function (cb) {
      try {
        var conn = mysql.createConnection(options.url);
        conn.connect(function (err) {
              try {
                if (err) {
                  logger.severe("could not connect to Database: " + options.url);
                  throw err;
                }

                self.setClient(conn);
                return cb(null);
              }
              catch (ex) {
                cb(ex);
              }
            }
        );
      }
      catch (ex) {
        cb(ex);
      }
    };

    var createTable = function (cb) {
      try {
        var conn = self.getClient();

        var createStmt =
            " create table if not exists " + self.getContainer() +
            "(" +
            "   scope varchar(255)," +
            "   `key` mediumtext," +
            "   `value` mediumtext," +
            "   primary key(scope(255), `key`(512))" +
            ")" +
            "ENGINE=InnoDB;";

        conn.query(createStmt, function (err) {
          if (err) {
            logger.severe("Could not create table: " + options.container);
          }
          return cb(err);
        });
      }
      catch (ex) {
        cb(ex);
      }
    };

    async.waterfall(
        [
          connectToDB,
          createTable,
          function() {
            self.baseInit(options, self.toArray(arguments).pop());
          }
        ],
        function(ex) {
          callback(ex);
        }
    );
  }
  catch(ex) {
    callback(ex);
  }
};



StatsHandlerMySQL.prototype.loadScopeData = function (scope, callback) {
  try {

    var self = this;
    var client = self.getClient();
    var container = self.getContainer();

    var stmt = "SELECT `key`, `value` FROM " + container + " WHERE scope = ?;";
    client.query(stmt, [scope], function (err, rows) {
      try {
        if (err) {
          throw err;
        }

        if (rows.length == 0) {
          return callback(null);
        }

        var flatJson = {};
        rows.forEach(function (row) {
          var key = row["key"];
          var value = row["value"];
          flatJson[key] = value;
        });

        var scopeData = self.unflatten(scope, flatJson);
        return callback(null, scopeData);
      }
      catch (ex) {
        callback(ex);
      }
    });
  }
  catch (ex) {
    cb(ex);
  }
};

StatsHandlerMySQL.prototype.saveScopeData = function (scope, scopeData, callback) {
  try {
    var self = this;
    var args = self.toArray(arguments);
    callback =  self.defaultCallback(args).pop();

    var client = self.getClient();
    var container = self.getContainer();
    var values = [];

    var beginTransaction = function (cb) {
      try {
        client.beginTransaction(function (err) {
          cb(err)
        });
      }
      catch (ex) {
        cb(ex);
      }
    };

    var deleteKeys = function (cb) {
      try {
        var stmt = "delete from " + container + " where scope = " + client.escape(scope) + ";";
        client.query(stmt, function (err) {
          cb(err);
        });
      }
      catch (ex) {
        cb(ex);
      }
    };

    var insertKeys = function (cb) {
      try {
        var flatJson = self.flatten(scope, scopeData);
        Object.keys(flatJson).forEach(function (key) {
          values.push([scope, key, flatJson[key]]);
        });

        //I know this is ugly, for some reason the bulk values was not being interpreted correctly when passed as parameters
        //also, want to avoid having queries that are too long, so split the updates into chunks of 100 key-value pairs at a time
        var bulk = 100;
        var queries = [];
        while (values.length > 0) {
          var subset = values.splice(0, bulk);
          var stmt =
            "insert into " + container + "(scope, `key`, `value`) values " + client.escape(subset) +
            "on duplicate key update " +
            "  `key` = values(`key`), " +
            "  `value` = values(`value`);";
          queries.push(stmt);
        }

        async.eachSeries(
          queries,
          function (query, nextCb) {
            client.query(query, nextCb);
          },
          function (err) {
            cb(err);
          }
        );
      }
      catch (ex) {
        cb(ex);
      }
    };

    var commitTransaction = function (cb) {
      try {
        client.commit(function (err) {
          cb(err);
        });
      }
      catch (ex) {
        cb(ex);
      }
    };

    async.waterfall(
      [
        beginTransaction,
        deleteKeys,
        insertKeys,
        commitTransaction
      ],
      function (err) {
        if (err) {
          client.rollback(function () {
            return callback(err);
          });
          return;
        }
        return callback(err);
      }
    );

  }
  catch (ex) {
    callback(ex);
  }
};


module.exports = StatsHandlerMySQL;