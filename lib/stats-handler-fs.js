var util = require("util");
var StatsHandlerMem = require("./stats-handler-mem.js");
var log = require("util-logging");
var logger =  new log.ConsoleLogger().setLevel(log.Level.INFO);
var async = require("async");
var fs = require("fs");
var path = require("path");

var StatsHandlerFs = function() {
  var self = this;
  (self.super_ = StatsHandlerFs.super_).call(self);

  return self;
};

util.inherits(StatsHandlerFs, StatsHandlerMem);


StatsHandlerFs.prototype.parseUrl = function(str) {
  try {
    var self = this;

    var prefix = "file://";
    self.validateUrlPrefix(prefix, str);

    str = str.replace(prefix, "");

    var options = {};
    options.directory = str;

    return options;
  }
  catch(ex) {
    logger.severe("Could not parse url: " + str);
    throw ex;
  }
};

StatsHandlerFs.prototype.init = function(options, callback) {
  var self = this;
  options = options || {};
  callback = callback || function () {};

  try {

    var clientOptions = self.parseUrl(options.url);
    self.setClient(clientOptions);


    var createDir = function(cb) {
      try {
        fs.stat(clientOptions.directory, function(err, fsStats) {
          try {
            //directory already exists
            if (fsStats && fsStats.isDirectory()) {
              return cb(null);
            }

            //there is something already there with the same name, but it's not a directory
            if (fsStats && !fsStats.isDirectory()) {
              throw new Error(util.format("path %s is not a directory", clientOptions.directory));
            }

            //some other error occurred
            if (err && err.code !== "ENOENT") {
              throw err;
            }

            //directory does not exist, create it
            fs.mkdir(clientOptions.directory, function(err) {
              if (err) {
                logger.severe("Could not create directory: %s", clientOptions.directory);
              }
              return cb(err);
            });
          }
          catch(ex) {
            cb(ex);
          }
        });
      }
      catch (ex) {
        cb(ex);
      }
    };

    async.waterfall(
        [
          createDir,
          function() {
            self.baseInit(options, self.toArray(arguments).pop());
          }
        ],
        function(ex) {
          callback(ex);
        }
    );
  }
  catch(ex) {
    callback(ex);
  }
};


StatsHandlerFs.prototype.makeScopeDataFilePath = function(scope) {
  var self = this;
  var client = self.getClient();

  var filePath = path.join(client.directory, scope) + ".json";
  return filePath;
};

StatsHandlerFs.prototype.loadScopeData = function(scope, callback) {
  try {
    var self = this;
    var path = self.makeScopeDataFilePath(scope);

    fs.readFile(path, function(err, data) {
      try {
        if (err && err.code === "ENOENT") {
          return callback(null);
        }

        //some other error occurred
        if (err) {
          throw err;
        }

        var scopeData = JSON.parse(data);
        return callback(null, scopeData);
      }
      catch(ex) {
        callback(ex);
      }
    });

  }
  catch (ex) {
    callback(ex);
  }
};

StatsHandlerFs.prototype.saveScopeData = function(scope, scopeData, callback) {
  try {
    var self = this;
    var path = self.makeScopeDataFilePath(scope);

    fs.writeFile(path,  JSON.stringify(scopeData, null, 2), function(err) {
      return callback(err);
    });
  }
  catch (ex) {
    callback(ex);
  }
};

module.exports = StatsHandlerFs;