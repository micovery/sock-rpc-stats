var wait = require("wait.for-es6");
var util = require("util");

exports.extend = function(repl, stats) {
  const saveScopeFunc = "_saveScope";
  const writeKeyFunc = "writeKey";
  const writeKeysFunc = "writeKeys";
  const readKeysFunc = "readKeys";
  const readKeyFunc = "readKey";
  const flushScopesFunc = "flushScopes";
  const wipeScopeFunc = "wipeScope";

  const metaFunc = "meta";


  var context = repl.context;

  context.help = function(section) {

    var help = {
      get:
          '  get();                                    // Get the names of all active scopes' + "\n" +
          '  get("scope");                             // Get all keys from a specific scope' + "\n" +
          '  get("scope", "key1", default);            // Get the value of a key from a specific scope' + "\n" +
          '  get("scope", ["key1", default], ...);     // Get the value of one or more keys from a specific scope',
      set:
          '  set("scope", "key", value);               // Update/Insert the given key-value pair within a specific scope' + "\n" +
          '  set("scope", ["key", value], ...);        // Update/Insert one or more key-value pairs within a specific scope',

      pop:
          '  pop("scope", "key1", default);            // remove, and get the last element of the array at the specified key' + "\n" +
          '  pop("scope", ["key1", default], ...);     // remove, and get the last element of one or more arrays at the specified keys ',

      shift:
          '  shift("scope", "key1", default);          // remove, and get the first element the array at the specified key' + "\n" +
          '  shift("scope", ["key1", default], ...);   // remove, and get the first element of one or more arrays ',

      count:
          '  count("scope");                           // count the number of child keys within the specified scope' + "\n" +
          '  count("scope", "key1", default);          // count the number of child keys within the specified key' + "\n" +
          '  count("scope", ["key1", default], ...);   // count the number of child keys for one or more specified keys ',

      keys:
          '  keys("scope");                           // get an array with the names of child keys within the specified scope' + "\n" +
          '  keys("scope", "key1", default);          // get an array with the names of child keys within the specified key' + "\n" +
          '  keys("scope", ["key1", default], ...);   // get an array with the names of child keys, for one ore more specified keys',

      push:
          '  push("scope", "key1", value);             // append the value to the end of the array at the specified key' + "\n" +
          '  push("scope", ["key1", value], ...);      // append the one or more values to the end of the arrays at the specified keys ',

      unshift:
          '  unshift("scope", "key1", value);          // prepend the value to the beginning of the array at the specified key' + "\n" +
          '  unshift("scope", ["key1", value], ...);   // prepend the one or more values to the beginning of the arrays at the specified keys ',

      merge:
          '  merge("scope", "key1", value);            // merges the value with the data at the specified key' + "\n" +
          '  merge("scope", ["key1", value], ...);     // merge the one or more values with the data at the specified keys ',

      flush:
          '  flush("scope", ...);                      // Save one or more scopes, and delete them from memory',

      wipe:
          '  wipe("scope");                            // Wipes all the keys of a specific scope',

      meta:
          '  meta("scope");                            // Get meta-data for a specific active scope',
      help:
          '  help();                                   // Show help for all sections' + "\n" +
          '  help("section", ...);                     // Show help for specific sections'
    };


    var sections = [];
    var args =  Array.prototype.slice.call(arguments, 0);
    if (args.length == 0) {
      args = Object.keys(help);
    }

    args.forEach(function(section) {
      var text = help[section];
      if (text) {
        console.log(text);
        console.log("");
      }
    });

    if (args.indexOf("help") < 0) {
      console.log(help.help);
    }

    return true;
  };


  var defaultCallback = function(args, cb) {
    if (!cb || typeof cb !== "function") {
      cb = function() {};
    }

    if (!args || !util.isArray(args)) {
      return [cb];
    }

    if (typeof args[args.length -1]  !== "function") {
      args.push(cb);
    }

    return args;
  };

  var read = function () {
    var self = this;
    var args = Array.prototype.slice.call(arguments, 0);
    var cb = defaultCallback(args).pop();
    var operation = args.shift();

    wait.launchFiber(function*(){
      //get(), keys()
      if (["get", "keys"].indexOf(operation) >= 0 && args.length == 0) {
        //get the list of all scopes
        var keys = Object.keys(stats.getData() || {});
        console.log(JSON.stringify(keys, null, 2));
        return cb(true);
      }

      //get(scope)
      if (operation == "get" && args.length == 1 && typeof args[0] == "string") {
        args.unshift(operation);
        args.unshift(readKeysFunc);
        args.unshift(stats);

        var result = yield wait.forMethod.apply(this, args);
        return cb(JSON.stringify(result, null, 2));
      }

      //count(scope), keys(scope)
      if (["count", "keys"].indexOf(operation) >= 0 && args.length == 1 && typeof args[0] == "string") {
        args.unshift(operation);
        args.unshift(readKeyFunc);
        args.unshift(stats);
        args.push(".");

        var result = yield wait.forMethod.apply(this, args);
        return cb(JSON.stringify(result, null, 2));
      }

      //operation(scope, key, default);
      if (args.length >= 2 && typeof args[0] == "string" && typeof args[1] == "string") {
        args.unshift(operation);
        args.unshift(readKeyFunc);
        args.unshift(stats);

        var result = yield wait.forMethod.apply(this, args);
        return cb(JSON.stringify(result, null, 2));
      }

      //operation(scope, pair, ...);
      if (args.length >= 2 && typeof args[0] == "string" && util.isArray(args[1])) {
        args.unshift(operation);
        args.unshift(readKeysFunc);
        args.unshift(stats);

        var result = yield wait.forMethod.apply(this, args);
        return cb(JSON.stringify(result, null, 2));
      }

      return cb("Unknown method signature. Use help(\"" + operation + "\") to see usage.");
    });
  };

  var sync1 = function(self, args) {
    wait.launchFiber(function *() {
      yield wait.for.apply(self, args);
    });
  };

  var sync2 = function(fun) {
    wait.launchFiber(function *() {
      yield wait.for(fun);
    });
  };

  context.get = function() {
    var self = this;
    var args =  Array.prototype.slice.call(arguments, 0);
    args.unshift("get");
    args.unshift(read);
    sync1(self, args);
  };

  context.shift = function() {
    var self = this;
    var args =  Array.prototype.slice.call(arguments, 0);
    args.unshift("shift");
    args.unshift(read);
    sync1(self, args);
  };

  context.pop = function() {
    var self = this;
    var args =  Array.prototype.slice.call(arguments, 0);
    args.unshift("pop");
    args.unshift(read);
    sync1(self, args);
  };

  context.count = function() {
    var self = this;
    var args =  Array.prototype.slice.call(arguments, 0);
    args.unshift("count");
    args.unshift(read);
    sync1(self, args);
  };

  context.keys = function() {
    var self = this;
    var args =  Array.prototype.slice.call(arguments, 0);
    args.unshift("keys");
    args.unshift(read);
    sync1(self, args);
  };

  var write = function () {
    var self = this;
    var args =  Array.prototype.slice.call(arguments, 0);
    var cb = defaultCallback(args).pop();
    var operation = args.shift();

    wait.launchFiber(function *() {
      //set(scope)
      if (operation == "set" && args.length == 1 && typeof args[0] == "string") {
        var scope = args.shift();

        yield wait.forMethod(stats, readKeysFunc, "get", scope);
        yield wait.forMethod(stats, saveScopeFunc, scope);
        return cb(true);
      }

      //set(scope, key value)
      if (args.length == 3 && typeof args[0] == "string" && typeof args[1] == "string") {
        var scope = args.shift();
        var key = args.shift();
        var value = args.shift();

        yield wait.forMethod(stats, writeKeyFunc, operation, scope, key, value);
        yield wait.forMethod(stats, saveScopeFunc, scope);
        return cb(true);
      }

      //set(scope, pair,...)
      if (args.length >= 2 && typeof args[0] == "string" && util.isArray(args[1])) {
        var scope = args[0];

        args.unshift(operation);
        args.unshift(writeKeysFunc);
        args.unshift(stats);

        yield wait.forMethod.apply(this, args);
        yield wait.forMethod(stats, saveScopeFunc, scope);
        return cb(true);
      }

      return cb("Unknown method signature. Use help(\"" + operation + "\") to see usage.");
    });
  };

  context.set = function() {
    var self = this;
    var args =  Array.prototype.slice.call(arguments, 0);
    args.unshift("set");
    args.unshift(write);

    sync1(self, args);
  };

  context.push = function() {
    var self = this;
    var args =  Array.prototype.slice.call(arguments, 0);
    args.unshift("push");
    args.unshift(write);
    sync1(self, args);
  };

  context.unshift = function() {
    var self = this;
    var args =  Array.prototype.slice.call(arguments, 0);
    args.unshift("unshift");
    args.unshift(write);
    sync1(self, args);
  };

  context.merge = function() {
    var self = this;
    var args =  Array.prototype.slice.call(arguments, 0);
    args.unshift("merge");
    args.unshift(write);
    sync1(self, args);
  };


  context.flush = function() {
    var self = this;
    var args =  Array.prototype.slice.call(arguments, 0);

    sync2(function(cb) {
      wait.launchFiber(function *() {
        //flush(scope,...)
        if (args.length >= 1 && typeof args[0] == "string") {
          args.unshift(flushScopesFunc);
          args.unshift(stats);
          var result = yield wait.forMethod.apply(this, args);
          return cb(result);
        }

        return cb("Unknown method signature. Use help(\"flush\") to see usage.");
      });
    });
  };

  context.wipe = function() {
    var self = this;
    var args =  Array.prototype.slice.call(arguments, 0);

    sync2(function(cb) {
      wait.launchFiber(function *() {
        //wipe(scope)
        if (args.length == 1 && typeof args[0] == "string") {
          var scope = args.shift();

          args.unshift(scope);
          args.unshift(wipeScopeFunc);
          args.unshift(stats);
          var result = yield wait.forMethod.apply(this, args);
          yield wait.forMethod(stats, saveScopeFunc, scope);
          return cb(result);
        }

        return cb("Unknown method signature. Use help(\"wipe\") to see usage.");
      });
    });
  };

  context.meta = function() {
    var self = this;
    var args =  Array.prototype.slice.call(arguments, 0);
    var cb = defaultCallback(args).pop();

    sync2(function(cb) {
      wait.launchFiber(function *() {
        //flush(scope,...)
        if (args.length == 1 && typeof args[0] == "string") {
          args.unshift(metaFunc);
          args.unshift(stats);
          var result = yield wait.forMethod.apply(this, args);
          return cb(result);
        }

        return cb("Unknown method signature. Use help(\"meta\") to see usage.");
      });
    });
  };

  //override the evaluation function so that commands run in a fiber
  var original = repl.eval;
  repl.eval = function (cmd, context, filename, callback) {
    wait.launchFiber(function *() {
      yield wait.for(original, cmd, context, filename);
    });
  };

  return context;
};

