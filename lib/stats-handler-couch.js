var util = require("util");
var StatsHandlerMem = require("./stats-handler-mem.js");
var log = require("util-logging");
var logger =  new log.ConsoleLogger().setLevel(log.Level.INFO);
var cradle = require("cradle");
var async = require("async");
var url = require("url");


var StatsHandlerCouch = function() {
  var self = this;
  (self.super_ = StatsHandlerCouch.super_).call(self);
  return self;
};

util.inherits(StatsHandlerCouch, StatsHandlerMem);


StatsHandlerCouch.prototype.logErrors = function(err) {
  if (err && util.isError(err)) {
    logger.severe(err);
  }
};

var trimChar = function(text, char) {
  if (!text) {
    return "";
  }
  text = text.replace(new RegExp("^"+ char + "+"),"");
  text = text.replace(new RegExp(""+ char +"+$"), "");
  return text;
};

StatsHandlerCouch.prototype.parseUrl = function(str) {

  try {
    var self = this;
    self.validateUrlPrefix(["http://", "https://"], str);

    var data = url.parse(str, true);
    var options = {};

    if (!data.pathname) {
      throw new Error("No database name provided in url")
    }

    if (!data.hostname) {
      throw new Error("No host provided in url");
    }

    options.host = data.hostname.trim();
    options.database = trimChar(data.pathname, "\\/").split("/")[0];

    options.extraOptions = {};
    if (data.auth) {
      var authParts = data.auth.split(":");
      if (authParts.length < 2) {
        throw new Error("No password in url");
      }

      options.extraOptions.auth = {};
      options.extraOptions.auth.username = authParts[0];
      options.extraOptions.auth.password = authParts[1];
    }


    if (!data.port) {
      if (data.protocol == "http:") {
        data.port = 90;
      }
      else if (data.protocol == "https:") {
        data.port = 443;
      }
      logger.info("No port specified in URL, using: %s", data.port);
      logger.info("CouchDB usually listens on 5984 for http://, or 6984 for https://");
    }
    options.port = data.port;

    options.extraOptions.cache = false; //this is on purpose, do not enable the cache because it causes save conflicts even with forceSave
    options.extraOptions.forceSave = true;
    options.extraOptions.secure =  (data.protocol == "https:");
    options.extraOptions.retries = 3;
    options.extraOptions.retryTimeout = 30 * 1000;

    return options;
  }
  catch(ex) {
    logger.severe("Could not parse url: " + str);
    throw ex;
  }
};

StatsHandlerCouch.prototype.init = function(options, callback) {
  var self = this;
  options = options || {};
  callback = callback || function () {};

  try {

    var clientOptions =  self.parseUrl(options.url);
    console.log(clientOptions);

    var connections = 0;
    var connectToDB = function (cb) {
      try {
        var con = new(cradle.Connection)(
            clientOptions.host,
            clientOptions.port,
            clientOptions.extraOptions);

        var db = con.database(clientOptions.database);
        self.setClient(db);

        db.exists(function(err, exists){
          try {
            if (err) {
              throw err;
            }

            if (!exists) {
              throw new Error("database \"" + clientOptions.database + "\" does not exist");
            }
            return cb(null);
          }
          catch(ex) {
            cb(ex);
          }
        });
      }
      catch (ex) {
        cb(ex);
      }
    };

    async.waterfall(
        [
          connectToDB,
          function () {
            self.baseInit(options, self.toArray(arguments).pop());
          }
        ],
        function (ex) {
          callback(ex);
        }
    );
  }
  catch(ex) {
    callback(ex);
  }
};


var isStatus = function(err, status) {
  if (!status || !err || !err.headers) {
    return false;
  }

  return (err.headers.status == status);
};

var is404 = function(err) {return isStatus(err, 404);};

StatsHandlerCouch.prototype.loadScopeData = function(scope, callback) {
  try {
    var self = this;

    callback = callback || function() {};

    var client = self.getClient();

    client.get(scope, function(err, doc) {
      try {
        if (err && !is404(err)) {
          return callback(err);
        }

        if (!doc) {
          return callback(null);
        }

        var scopeData = doc.json;
        delete scopeData._rev;
        return callback(null, scopeData);
      }
      catch (ex) {
        callback(ex);
      }
    });
  }
  catch (ex) {
    callback(ex);
  }
};

StatsHandlerCouch.prototype.saveScopeData = function(scope, scopeData, callback) {
  try {
    var self = this;
    callback = callback || function() {};

    var client = self.getClient();

    if (!client) {
      throw new Error("no client connection available");
    }

    //let cradle handle the IDs
    delete scopeData._id;
    delete scopeData._rev;

    client.save(scope, scopeData, function(err, result) {
      return callback(err);
    });
  }
  catch (ex) {
    callback(ex);
  }
};



module.exports = StatsHandlerCouch;